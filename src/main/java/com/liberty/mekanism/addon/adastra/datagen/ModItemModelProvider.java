package com.liberty.mekanism.addon.adastra.datagen;

import net.minecraft.data.PackOutput;
import net.minecraftforge.client.model.generators.ItemModelProvider;
import net.minecraftforge.common.data.ExistingFileHelper;

import static com.liberty.mekanism.addon.adastra.Consts.MOD_ID;

public class ModItemModelProvider extends ItemModelProvider {
    public ModItemModelProvider(PackOutput output, ExistingFileHelper existingFileHelper) {
        super(output, MOD_ID, existingFileHelper);
    }

    @Override
    protected void registerModels() {
        // TODO: all items currently use the same texture as ore blocks, so this isn't needed.
//        MOD_ITEMS.forEach((k, v) -> withExistingParent(v.getId().getPath(),
//                new ResourceLocation("item/generated")).texture("layer0",
//                new ResourceLocation(MOD_ID, "item/" + v.getId().getPath())));
    }
}
