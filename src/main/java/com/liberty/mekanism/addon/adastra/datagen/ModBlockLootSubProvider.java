package com.liberty.mekanism.addon.adastra.datagen;

import com.liberty.mekanism.addon.adastra.OreRegister;
import mekanism.common.registries.MekanismItems;
import mekanism.common.resource.PrimaryResource;
import mekanism.common.resource.ResourceType;
import net.minecraft.data.loot.BlockLootSubProvider;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.functions.ApplyBonusCount;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;
import net.minecraftforge.registries.RegistryObject;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Set;

public class ModBlockLootSubProvider extends BlockLootSubProvider {
    protected ModBlockLootSubProvider() {
        super(Set.of(), FeatureFlags.REGISTRY.allFlags());
    }

    @Override
    protected void generate() {
        OreRegister.BLOCKS.getEntries().forEach((v) -> {
            LootTable.Builder builder = switch (Objects.requireNonNull(OreRegister.MOD_ORE_IDS.get(v.getId()))) {
                // See https://github.com/mekanism/Mekanism/blob/1.20.x/src/datagen/main/java/mekanism/common/loot/table/MekanismBlockLootTables.java
                case FLUORITE -> createSilkTouchDispatchTable(v.get(),
                        applyExplosionDecay(v.get(),
                                LootItem.lootTableItem(MekanismItems.FLUORITE_GEM)
                                        .apply(SetItemCountFunction.setCount(UniformGenerator.between(2, 4)))
                                        .apply(ApplyBonusCount.addOreBonusCount(Enchantments.BLOCK_FORTUNE))));
                case TIN -> createOreDrop(v.get(),
                        Objects.requireNonNull(MekanismItems.PROCESSED_RESOURCES.get(ResourceType.RAW,
                                PrimaryResource.TIN)).get());
                case OSMIUM -> createOreDrop(v.get(),
                        Objects.requireNonNull(MekanismItems.PROCESSED_RESOURCES.get(ResourceType.RAW,
                                PrimaryResource.OSMIUM)).get());
                case LEAD -> createOreDrop(v.get(),
                        Objects.requireNonNull(MekanismItems.PROCESSED_RESOURCES.get(ResourceType.RAW,
                                PrimaryResource.LEAD)).get());
                case URANIUM -> createOreDrop(v.get(),
                        Objects.requireNonNull(MekanismItems.PROCESSED_RESOURCES.get(ResourceType.RAW,
                                PrimaryResource.URANIUM)).get());
            };
            add(v.get(), builder);
        });
    }

    @Override
    protected @NotNull Iterable<Block> getKnownBlocks() {
        return OreRegister.BLOCKS.getEntries().stream().map(RegistryObject::get)::iterator;
    }
}
