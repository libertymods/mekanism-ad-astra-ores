package com.liberty.mekanism.addon.adastra.datagen;

import com.liberty.mekanism.addon.adastra.OreRegister;
import net.minecraft.data.PackOutput;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.common.data.LanguageProvider;
import net.minecraftforge.registries.RegistryObject;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static com.liberty.mekanism.addon.adastra.Consts.MOD_ID;
import static com.liberty.mekanism.addon.adastra.Consts.MOD_NAME;
import static com.liberty.mekanism.addon.adastra.OreRegister.MOD_ORE_IDS;

public class ModLanguageProviders {
    public enum SupportedLocale {
        EN_US("en_us"),
        JA_JP("ja_jp"),
        KO_KR("ko_kr"),
        ZH_CN("zh_cn"),
        ZH_TW("zh_tw"),
        FR_FR("fr_fr"),
        DE_DE("de_de"),
        RU_RU("ru_ru"),
        UK_UA("uk_ua");
        private final String localeCode;

        SupportedLocale(String localeCode) {
            this.localeCode = localeCode;
        }
    }

    private static final String MOON = "moon";
    private static final String MARS = "mars";
    private static final String MERCURY = "mercury";
    private static final String VENUS = "venus";
    private static final String GLACIO = "glacio";

    public static List<LanguageProvider> getLanguageProviders(PackOutput packOutput) {
        return Arrays.stream(SupportedLocale.values())
                .map((loc) -> ModLanguageProviders.getLanguageProvider(packOutput, loc))
                .toList();
    }

    public static LanguageProvider getLanguageProvider(PackOutput packOutput, SupportedLocale locale) {
        Map<RegistryObject<Block>, List<String>> oreNames =
                OreRegister.BLOCKS.getEntries().stream().collect(Collectors.toMap((k) -> k,
                        (v) -> Arrays.stream(v.getId().getPath().split("_")).toList()));
        // How to update:
        // For planet names, see Ad Astra's lang files:
        // https://github.com/terrarium-earth/Ad-Astra/tree/1.20.x/common/src/main/resources/assets/ad_astra/lang
        // For ore names, see Mekanism's lang files:
        // https://github.com/mekanism/Mekanism/tree/1.20.x/src/main/resources/assets/mekanism/lang
        final Consumer<LanguageProvider> genLocale = switch (locale) {
            case EN_US -> (l) -> oreNames.forEach((k, v) -> l.add(k.get(),
                    v.stream().map(StringUtils::capitalize).collect(Collectors.joining(
                            " "))));
            case JA_JP -> (l) -> oreNames.forEach((k, v) -> {
                final String planet = switch (v.get(0)) {
                    case MOON -> "月";
                    case MARS -> "火星";
                    case MERCURY -> "水星";
                    case VENUS -> "金星";
                    case GLACIO -> "グレーシャー";
                    default -> v.get(0);
                };
                final String oreName = switch (Objects.requireNonNull(MOD_ORE_IDS.get(k.getId()))) {
                    case TIN -> "錫";
                    case OSMIUM -> "オスミウム";
                    case LEAD -> "鉛";
                    case URANIUM -> "ウラン";
                    case FLUORITE -> "蛍石";
                };
                l.add(k.get(), String.format("%s%s%s", planet, oreName, "鉱石"));
            });
            case KO_KR -> (l) -> oreNames.forEach((k, v) -> {
                // Special thanks to gisellevonbingen@ for the translations.
                final String planet = switch (v.get(0)) {
                    case MOON -> "달";
                    case MARS -> "화성";
                    case MERCURY -> "수성";
                    case VENUS -> "금성";
                    case GLACIO -> "글라시오";
                    default -> v.get(0);
                };
                final String oreName = switch (Objects.requireNonNull(MOD_ORE_IDS.get(k.getId()))) {
                    case TIN -> "주석";
                    case OSMIUM -> "오스뮴";
                    case LEAD -> "납";
                    case URANIUM -> "우라늄";
                    case FLUORITE -> "형석";
                };
                l.add(k.get(), String.format("%s %s %s", planet, oreName, "광석"));
            });
            case ZH_CN -> (l) -> oreNames.forEach((k, v) -> {
                final String planet = switch (v.get(0)) {
                    case MOON -> "月球";
                    case MARS -> "火星";
                    case MERCURY -> "水星";
                    case VENUS -> "金星";
                    case GLACIO -> "霜原";
                    default -> v.get(0);
                };
                final String oreName = switch (Objects.requireNonNull(MOD_ORE_IDS.get(k.getId()))) {
                    case TIN -> "锡";
                    case OSMIUM -> "锇";
                    case LEAD -> "铅";
                    case URANIUM -> "铀";
                    case FLUORITE -> "氟石";
                };
                l.add(k.get(), String.format("%s%s%s", planet, oreName, "矿石"));
            });
            case ZH_TW -> (l) -> oreNames.forEach((k, v) -> {
                final String planet = switch (v.get(0)) {
                    // TODO(liberty): Update these once Ad Astra has zh_tw.
                    case MOON -> "月球";
                    case MARS -> "火星";
                    case MERCURY -> "水星";
                    case VENUS -> "金星";
                    case GLACIO -> "霜原";
                    default -> v.get(0);
                };
                final String oreName = switch (Objects.requireNonNull(MOD_ORE_IDS.get(k.getId()))) {
                    case TIN -> "錫";
                    case OSMIUM -> "鋨";
                    case LEAD -> "鉛";
                    case URANIUM -> "鈾";
                    case FLUORITE -> "螢石";
                };
                l.add(k.get(), String.format("%s%s%s", planet, oreName, "礦"));
            });
            case FR_FR -> (l) -> oreNames.forEach((k, v) -> {
                final String planet = switch (v.get(0)) {
                    case MOON -> "Lunaire";
                    case MARS -> "Martienne";
                    case MERCURY -> "Mercurienne";
                    case VENUS -> "Venusienne";
                    // Ad Astra's fr_fr sometimes uses "Glacio" and other times "Glacienne"
                    case GLACIO -> "Glacienne";
                    default -> v.get(0);
                };
                final String oreName = switch (Objects.requireNonNull(MOD_ORE_IDS.get(k.getId()))) {
                    case TIN -> "d'étain";
                    case OSMIUM -> "d'osmium";
                    case LEAD -> "de plomb";
                    case URANIUM -> "d'uranium";
                    case FLUORITE -> "de fluorite";
                };
                l.add(k.get(), String.format("Minerais %s des %s", oreName, planet));
            });
            case DE_DE -> (l) -> oreNames.forEach((k, v) -> {
                final String planet = switch (v.get(0)) {
                    case MOON -> "Mond";
                    case MARS -> "Mars";
                    case MERCURY -> "Merkur";
                    case VENUS -> "Venus";
                    case GLACIO -> "Glacio";
                    default -> v.get(0);
                };
                final String oreName = switch (Objects.requireNonNull(MOD_ORE_IDS.get(k.getId()))) {
                    case TIN -> "Zinnerz";
                    case OSMIUM -> "Osmiumerz";
                    case LEAD -> "Bleierz";
                    case URANIUM -> "Uranerz";
                    case FLUORITE -> "Fluoriterz";
                };
                l.add(k.get(), String.format("%s %s", planet, oreName));
            });
            case RU_RU -> (l) -> oreNames.forEach((k, v) -> {
                final String planet = switch (v.get(0)) {
                    case MOON -> "Лунная";
                    case MARS -> "Марсианская";
                    case MERCURY -> "Меркуриевая";
                    case VENUS -> "Венерианская";
                    case GLACIO -> "Гласианская";
                    default -> v.get(0);
                };
                final String oreName = switch (Objects.requireNonNull(MOD_ORE_IDS.get(k.getId()))) {
                    case TIN -> "Оловянная";
                    case OSMIUM -> "Осмиевая";
                    case LEAD -> "Свинцовая";
                    case URANIUM -> "Урановая";
                    case FLUORITE -> "Флюоритовая";
                };
                l.add(k.get(), String.format("%s %s %s", planet, oreName, "руда"));
            });
            case UK_UA -> (l) -> oreNames.forEach((k, v) -> {
                final String planet = switch (v.get(0)) {
                    case MOON -> "Місячна";
                    case MARS -> "Марсова";
                    case MERCURY -> "Меркурієва";
                    case VENUS -> "Венеріанська";
                    case GLACIO -> "Льодовикова";
                    default -> v.get(0);
                };
                final String oreName = switch (Objects.requireNonNull(MOD_ORE_IDS.get(k.getId()))) {
                    case TIN -> "Олов'яна";
                    case OSMIUM -> "Осмієва";
                    case LEAD -> "Свинцева";
                    case URANIUM -> "Уранієва";
                    case FLUORITE -> "Флюоритова";
                };
                l.add(k.get(), String.format("%s %s %s", planet, oreName, "руда"));
            });

        };
        return new LanguageProvider(packOutput, MOD_ID, locale.localeCode) {
            @Override
            protected void addTranslations() {
                // mod name is english only.
                add("itemGroup.mekanismaaa.ores", MOD_NAME);
                genLocale.accept(this);
            }
        };
    }

}
