package com.liberty.mekanism.addon.adastra.datagen;

import mekanism.common.resource.ore.OreType;
import net.minecraft.resources.ResourceLocation;

import java.util.Objects;

import static com.liberty.mekanism.addon.adastra.OreRegister.MOD_ORE_IDS;

// Common tag logic for items and blocks.
public class Util {
    public static boolean isOreRatesDense(ResourceLocation k) {
        return Objects.requireNonNull(MOD_ORE_IDS.get(k)).equals(OreType.FLUORITE);
    }

    public static ResourceLocation oresInGround(ResourceLocation k) throws AssertionError {
        if (k.getPath().startsWith("moon_")) {
            return new ResourceLocation("forge", "ores_in_ground/moon_stone");
        } else if (k.getPath().startsWith("mars_")) {
            return new ResourceLocation("forge", "ores_in_ground/mars_stone");
        } else if (k.getPath().startsWith("venus_")) {
            return new ResourceLocation("forge", "ores_in_ground/venus_stone");
        } else if (k.getPath().startsWith("mercury_")) {
            return new ResourceLocation("forge", "ores_in_ground/mercury_stone");
        } else if (k.getPath().startsWith("glacio_")) {
            return new ResourceLocation("forge", "ores_in_ground/glacio_stone");
        } else {
            throw new AssertionError(String.format("Can't make ores_in_ground/ item tag for %s", k));
        }
    }

    public static ResourceLocation ores(ResourceLocation k) throws AssertionError {
        return switch (Objects.requireNonNull(MOD_ORE_IDS.get(k))) {
            case OSMIUM -> new ResourceLocation("forge", "ores/osmium");
            case TIN -> new ResourceLocation("forge", "ores/tin");
            case LEAD -> new ResourceLocation("forge", "ores/lead");
            case URANIUM -> new ResourceLocation("forge", "ores/uranium");
            case FLUORITE -> new ResourceLocation("forge", "ores/fluorite");
        };
    }
}
