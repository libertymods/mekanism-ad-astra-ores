package com.liberty.mekanism.addon.adastra.datagen;

import com.liberty.mekanism.addon.adastra.Consts;
import com.liberty.mekanism.addon.adastra.OreRegister;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.data.BlockTagsProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.CompletableFuture;

public class ModBlockTagsProvider extends BlockTagsProvider {
    public ModBlockTagsProvider(PackOutput output, CompletableFuture<HolderLookup.Provider> lookupProvider,
                                @Nullable ExistingFileHelper existingFileHelper) {
        super(output, lookupProvider, Consts.MOD_ID, existingFileHelper);
    }

    @Override
    protected void addTags(HolderLookup.@NotNull Provider provider) {
        OreRegister.BLOCKS.getEntries().forEach((v) -> {
            // forge:ores
            this.tag(Tags.Blocks.ORES).add(v.get());
            // forge:ores/...
            this.tag(BlockTags.create(Util.ores(v.getId()))).add(v.get());
            // forge:ore_rates/...
            if (Util.isOreRatesDense(v.getId())) {
                this.tag(Tags.Blocks.ORE_RATES_DENSE).add(v.get());
            } else {
                this.tag(Tags.Blocks.ORE_RATES_SINGULAR).add(v.get());
            }
            // forge:ores_in_ground/...
            this.tag(BlockTags.create(Util.oresInGround(v.getId()))).add(v.get());
            // minecraft:mineable/pickaxe
            this.tag(BlockTags.MINEABLE_WITH_PICKAXE).add(v.get());
            // minecraft:needs_stone_tool
            this.tag(BlockTags.NEEDS_STONE_TOOL).add(v.get());
            // minecraft:snaps_goat_horn
            this.tag(BlockTags.SNAPS_GOAT_HORN).add(v.get());
            // forge:mineable/paxel
            this.tag(BlockTags.create(new ResourceLocation("forge", "mineable/paxel"))).add(v.get());
            // minecraft:overworld_carver_replaceables
            this.tag(BlockTags.OVERWORLD_CARVER_REPLACEABLES).add(v.get());
            // mekanism:atomic_disassembler_ore
            this.tag(BlockTags.create(new ResourceLocation("mekanism", "atomic_disassembler_ore"))).add(v.get());
        });
    }
}
