package com.liberty.mekanism.addon.adastra.datagen;

import com.liberty.mekanism.addon.adastra.OreRegister;
import net.minecraft.data.PackOutput;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.common.data.ExistingFileHelper;

import static com.liberty.mekanism.addon.adastra.Consts.MOD_ID;

public class ModBlockStateProvider extends BlockStateProvider {

    public ModBlockStateProvider(PackOutput output, ExistingFileHelper exFileHelper) {
        super(output, MOD_ID, exFileHelper);
    }

    @Override
    protected void registerStatesAndModels() {
        OreRegister.BLOCKS.getEntries().forEach((v) -> simpleBlockWithItem(v.get(), cubeAll(v.get())));
    }
}
