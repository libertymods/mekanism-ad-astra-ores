package com.liberty.mekanism.addon.adastra.datagen;

import com.liberty.mekanism.addon.adastra.OreRegister;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.data.ExistingFileHelper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.CompletableFuture;

import static com.liberty.mekanism.addon.adastra.Consts.*;
import static com.liberty.mekanism.addon.adastra.datagen.Util.*;

public class ModItemTagsProvider extends ItemTagsProvider {
    public ModItemTagsProvider(PackOutput arg, CompletableFuture<HolderLookup.Provider> completableFuture,
                               CompletableFuture<TagLookup<Block>> completableFuture2,
                               @Nullable ExistingFileHelper existingFileHelper) {
        super(arg, completableFuture, completableFuture2, MOD_ID, existingFileHelper);
    }

    @Override
    protected void addTags(HolderLookup.@NotNull Provider provider) {
        OreRegister.ITEMS.getEntries().forEach((v) -> {
            // forge:ores
            this.tag(Tags.Items.ORES).add(v.get());
            // forge:ores/...
            this.tag(ItemTags.create(ores(v.getId()))).add(v.get());
            // forge:ore_rates/...
            if (isOreRatesDense(v.getId())) {
                this.tag(Tags.Items.ORE_RATES_DENSE).add(v.get());
            } else {
                this.tag(Tags.Items.ORE_RATES_SINGULAR).add(v.get());
            }
            // forge:ores_in_ground/...
            this.tag(ItemTags.create(oresInGround(v.getId()))).add(v.get());
        });
    }
}
