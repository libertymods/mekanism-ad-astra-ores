package com.liberty.mekanism.addon.adastra;

import net.minecraft.resources.ResourceLocation;

public final class Consts {
    public static final String MOD_ID = "mekanismaaa";
    public static final String MOD_NAME = "Mekanism: Ad Astra Ores";

    // <editor-fold desc="mod item/block ids">
    public static final ResourceLocation GLACIO_FLUORITE_ORE = new ResourceLocation(MOD_ID, "glacio_fluorite_ore");
    public static final ResourceLocation GLACIO_LEAD_ORE = new ResourceLocation(MOD_ID, "glacio_lead_ore");
    public static final ResourceLocation GLACIO_OSMIUM_ORE = new ResourceLocation(MOD_ID, "glacio_osmium_ore");
    public static final ResourceLocation GLACIO_TIN_ORE = new ResourceLocation(MOD_ID, "glacio_tin_ore");
    public static final ResourceLocation GLACIO_URANIUM_ORE = new ResourceLocation(MOD_ID, "glacio_uranium_ore");
    public static final ResourceLocation MARS_FLUORITE_ORE = new ResourceLocation(MOD_ID, "mars_fluorite_ore");
    public static final ResourceLocation MARS_TIN_ORE = new ResourceLocation(MOD_ID, "mars_tin_ore");
    public static final ResourceLocation MARS_URANIUM_ORE = new ResourceLocation(MOD_ID, "mars_uranium_ore");
    public static final ResourceLocation MERCURY_LEAD_ORE = new ResourceLocation(MOD_ID, "mercury_lead_ore");
    public static final ResourceLocation MERCURY_OSMIUM_ORE = new ResourceLocation(MOD_ID, "mercury_osmium_ore");
    public static final ResourceLocation MERCURY_URANIUM_ORE = new ResourceLocation(MOD_ID, "mercury_uranium_ore");
    public static final ResourceLocation MOON_OSMIUM_ORE = new ResourceLocation(MOD_ID, "moon_osmium_ore");
    public static final ResourceLocation MOON_TIN_ORE = new ResourceLocation(MOD_ID, "moon_tin_ore");
    public static final ResourceLocation VENUS_FLUORITE_ORE = new ResourceLocation(MOD_ID, "venus_fluorite_ore");
    public static final ResourceLocation VENUS_LEAD_ORE = new ResourceLocation(MOD_ID, "venus_lead_ore");
    public static final ResourceLocation VENUS_URANIUM_ORE = new ResourceLocation(MOD_ID, "venus_uranium_ore");
    // </editor-fold>
}