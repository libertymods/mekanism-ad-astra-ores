package com.liberty.mekanism.addon.adastra;

import com.google.common.collect.ImmutableMap;
import mekanism.common.registries.MekanismBlocks;
import mekanism.common.resource.ore.OreType;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class OreRegister {
    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, Consts.MOD_ID);
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, Consts.MOD_ID);

    /**
     * Map of ores introduced by the mod.
     * Key is the name of the ore to add, value is the equivalent ore in Mekanism.
     */
    public static final ImmutableMap<ResourceLocation, OreType> MOD_ORE_IDS = ImmutableMap.<ResourceLocation,
                    OreType>builder()
            .put(Consts.GLACIO_FLUORITE_ORE, OreType.FLUORITE)
            .put(Consts.GLACIO_LEAD_ORE, OreType.LEAD)
            .put(Consts.GLACIO_OSMIUM_ORE, OreType.OSMIUM)
            .put(Consts.GLACIO_TIN_ORE, OreType.TIN)
            .put(Consts.GLACIO_URANIUM_ORE, OreType.URANIUM)
            .put(Consts.MARS_FLUORITE_ORE, OreType.FLUORITE)
            .put(Consts.MARS_TIN_ORE, OreType.TIN)
            .put(Consts.MARS_URANIUM_ORE, OreType.URANIUM)
            .put(Consts.MERCURY_LEAD_ORE, OreType.LEAD)
            .put(Consts.MERCURY_OSMIUM_ORE, OreType.OSMIUM)
            .put(Consts.MERCURY_URANIUM_ORE, OreType.URANIUM)
            .put(Consts.MOON_OSMIUM_ORE, OreType.OSMIUM)
            .put(Consts.MOON_TIN_ORE, OreType.TIN)
            .put(Consts.VENUS_FLUORITE_ORE, OreType.FLUORITE)
            .put(Consts.VENUS_LEAD_ORE, OreType.LEAD)
            .put(Consts.VENUS_URANIUM_ORE, OreType.URANIUM)
            .build();

    private static final Map<ResourceLocation, RegistryObject<Block>> MOD_BLOCKS =
            MOD_ORE_IDS.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
                    (entry) -> BLOCKS.register(entry.getKey().getPath(), () -> {
                        Block from = Objects.requireNonNull(MekanismBlocks.ORES.get(entry.getValue()).deepslateBlock());
                        // There may be a better way to detect typos or missing blocks...
                        if (from.getDescriptionId().equals(Blocks.AIR.getDescriptionId())) {
                            throw new UnsupportedOperationException(String.format("Can't find equivalent block: %s. " +
                                    "Is Mekanism installed?", entry.getValue()));
                        }
                        return new Block(BlockBehaviour.Properties.copy(from));
                    })));

    private static final Map<ResourceLocation, RegistryObject<Item>> MOD_ITEMS =
            MOD_BLOCKS.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
                    (entry) -> ITEMS.register(entry.getKey().getPath(), () -> new BlockItem(entry.getValue().get(),
                            new Item.Properties()))));

    private static final DeferredRegister<CreativeModeTab> CREATIVE_MODE_TABS =
            DeferredRegister.create(Registries.CREATIVE_MODE_TAB, Consts.MOD_ID);

    /**
     * Registers the mod ores and adds them in creative mode tab.
     *
     * @param bus The mod event bus.
     */
    public static void register(IEventBus bus) {
        if (ITEMS.getEntries().isEmpty()) {
            ForgeMod.LOGGER.warn("No items registered!");
        }
        CREATIVE_MODE_TABS.register("items", () -> CreativeModeTab.builder().title(Component.translatable("itemGroup" +
                "." + Consts.MOD_ID + ".ores")).icon(MOD_ITEMS.get(Consts.MARS_URANIUM_ORE).get()::getDefaultInstance).displayItems(
                (params, output) -> ITEMS.getEntries().stream().sorted((m1, m2) ->
                                String.CASE_INSENSITIVE_ORDER.compare(m1.getId().getPath(), m2.getId().getPath()))
                        .forEach((v) -> output.accept(v.get()))).build());
        BLOCKS.register(bus);
        ITEMS.register(bus);
        CREATIVE_MODE_TABS.register(bus);
        ForgeMod.LOGGER.info("Registered {} items from the mod", ITEMS.getEntries().size());
    }
}
