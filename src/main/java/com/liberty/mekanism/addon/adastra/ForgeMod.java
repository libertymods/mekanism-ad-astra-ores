package com.liberty.mekanism.addon.adastra;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(Consts.MOD_ID)
public class ForgeMod {
    public static final Logger LOGGER = LogManager.getLogger();

    public ForgeMod() {
        IEventBus MOD_BUS = FMLJavaModLoadingContext.get().getModEventBus();
        OreRegister.register(MOD_BUS);
        MinecraftForge.EVENT_BUS.register(this);
    }
}
