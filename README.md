Space is too boring? Not if you're a mining enterprise!

## Features
- Makes Mekanism ores (tin, osmium, lead, uranium, fluorite) spawn on all five Ad Astra planets.
- Custom painted textures for all the ore variants.
- Functionally equivalent: fully compatible with all Mekanism machines and alike.
- Makes deepslate variants of Mekanism ores spawn naturally in Glacio's deepslate regions.

## Planet Resource Table
The below table shows which new ores are generated at which planets!

| Planet \ Ore | Tin | Osmium | Lead | Uranium | Fluorite |
|--------------|:---:|:------:|:----:|:-------:|:--------:|
| Moon         |  ✅  |   ✅    |      |         |          |
| Mars         |  ✅  |        |      |    ✅    |    ✅     |
| Mercury      |     |   ✅    |  ✅   |    ✅    |          |
| Venus        |     |        |  ✅   |    ✅    |    ✅     |
| Glacio       |  ✅  |   ✅    |  ✅   |    ✅    |    ✅     |

## FAQs
- How do I tweak the ore gen?
    - Mekanism world generation settings also apply to the mod ores' placement and amount.
    - Individual ore variant can be overridden via datapacks. See `data/mekanismaaa/worldgen/configured_feature` folder in the JAR file for details.
- How do I make Ad Astra ores (e.g. Desh) work with Mekanism ore-processing?
    - This feature is not included in the mod. I recommend *JAOPCA* or *More Mekanism Processing*.

*This mod is not affiliated with Mekanism or Ad Astra.*